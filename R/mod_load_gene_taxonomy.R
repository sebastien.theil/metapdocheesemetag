#' load_gene_taxonomy UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
mod_load_gene_taxonomy_ui <- function(id){
  ns <- NS(id)
  tagList(
    fluidPage(
      fluidRow(
        h1("Taxonomy table based on Emapper taxonomy of predicted genes."),
        DT::dataTableOutput(ns('gene_taxo_datatable'))
      ),
      fluidRow(
        box(title = "Settings", width = 12,
            shinyWidgets::pickerInput(
              inputId = ns('glom_rank'),
              label = "Choose rank to glom taxonomy table", 
              choices = c('superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specie'),
              selected = 'specie'
            )
        )
      ),
      fluidRow(
        h3("Taxonomy table agglomerate by selected rank."),
        DT::dataTableOutput(ns('gene_taxo_datatable_glom'))
      ),
      fluidRow(
        box(title = "Paramètres Heatmap", width = 12,
            h5("Selectionner les métadonnées à afficher sur la heatmap."),
            shinyWidgets::multiInput(
              inputId = ns('sample_metadata_colors'),
              label = 'Sample metadata (columns colors)',
              choices = 'AOP'
            ),
            shinyWidgets::multiInput(
              inputId = ns('gene_row_colors'),
              label = 'Genes taxonomy ranks (rows colors)',
              choices = 'specie'
            ),
            tags$h3("Plot size"),
            sliderInput(ns('plot_height'), label = 'Plot Height', min = 1000, max = 3000, step = 100, value = 1000),
            sliderInput(ns('plot_width'), label = 'Plot Width', min = 1000, max = 3000, step = 100, value= 1000)
        ),
        shinyWidgets::actionBttn(ns('launch_gene_taxo'), label = "Launch heatmap.", style = "material-flat")
      )
      
      
      
    ),
    uiOutput(ns('heatmap_ui'))
  )
}

#' load_gene_taxonomy Server Functions
#'
#' @noRd 
#' 
#' @importFrom dplyr group_by_at mutate distinct ungroup pull
mod_load_gene_taxonomy_server <- function(input, output, session, r){
  ns <- session$ns
  
  
  observe({
    req(r$submetadata())
    flog.info('updateMultiInput')
    shinyWidgets::updateMultiInput(session = session,
                                   inputId = 'sample_metadata_colors',
                                   choices = colnames(r$submetadata()),
                                   selected = 'AOP')
    flog.info('update done.')
  })
  
  observe({
    req(r$submetadata())
    taxon <- c('superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specie')
    flog.info('updateMultiInput')
    shinyWidgets::updateMultiInput(session = session,
                                   inputId = 'gene_row_colors',
                                   choices = taxon[1:which(taxon==input$glom_rank)],
                                   selected = 'specie')
    flog.info('update done.')
  })
  
  
  gene_taxo_table <- reactive({
    tt <- vroom::vroom(file = 'inst/data/gene_taxonomy.csv', delim = "\t")
    tt <- tidyr::separate(tt, taxid, sep=';', into = c('superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specie'))
    tt <- tt %>% dplyr::mutate_if(is.character, as.factor)
    
    tt[,unlist(lapply(tt, is.numeric))] <- tt[,unlist(lapply(tt, is.numeric))] %>% replace(is.na(.), 0)
    colsnames <- colnames(tt)
    no_AOP_cols <- stringr::str_detect(colsnames, "AOP")
    no_AOP_cols <- colsnames[!no_AOP_cols]
    selected.cols <- c(no_AOP_cols, as.vector(r$submetadata()$sample.id))
    tt <- tt %>% dplyr::select(selected.cols)
    sup.zero <- tt %>% dplyr::select_if(is.numeric) %>% rowSums() > 0
    tt <- tt[sup.zero,]
    return(tt)
  })
  
  
  output$gene_taxo_datatable <- DT::renderDataTable({
    gene_taxo_table()
  }, filter="top",options = list(pageLength = 10, scrollX = TRUE, server=TRUE))
  
  
  glom_table <- reactive({
    req(gene_taxo_table(), input$glom_rank, input$gene_taxo_datatable_rows_all)
    dt <- gene_taxo_table()
    dt <- dt %>% dplyr::slice(input$gene_taxo_datatable_rows_all)
    if(input$glom_rank == 'specie'){
      return(dt)
    }
    else{
      taxon <- c('superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specie')
      dt <- dt %>% select(taxon[1:which(taxon==input$glom_rank)], where(is.numeric)) %>% 
        dplyr::group_by_at(input$glom_rank) %>% 
        dplyr::mutate(across(where(is.numeric), sum)) %>% dplyr::distinct_at(input$glom_rank, .keep_all=TRUE) %>% dplyr::ungroup()
      return(dt)
    }
  })
  
  
  output$gene_taxo_datatable_glom <- DT::renderDataTable({
    glom_table()
  }, filter="top",options = list(pageLength = 10, scrollX = TRUE, server=TRUE))
  
  
  plot_height <- reactive({
    return(input$plot_height)
  })
  
  plot_width <- reactive({
    return(input$plot_width)
  })
  
  observeEvent(input$launch_gene_taxo,{
    output$heatmap_ui <- renderUI({
      req(plot_height(), plot_width())
      plotlyOutput(ns('heatmap_gene_taxo'), height = paste(plot_height(), 'px', sep = ''), width = paste(plot_width(), 'px', sep=''))
      # plotlyOutput(ns('heatmap_gene_taxo'), height = "1200px", width = "1200px")
    })
  })
  
  
  
  output$heatmap_gene_taxo <- renderPlotly({
    launch_heatmap()
  })
  
  launch_heatmap <- eventReactive(input$launch_gene_taxo,{
    dt <- glom_table()
    num.table <- log(dt %>% select(where(is.numeric)) + 0.00000001)
    rownames(num.table) <- dplyr::pull(dt,input$glom_rank)
    p <- heatmaply::heatmaply(
      num.table,
      row_side_colors = dt %>% dplyr::select_at(input$gene_row_colors),
      col_side_colors = r$submetadata() %>% dplyr::select_at(input$sample_metadata_colors)
    )
    
    return(p)
  })
  
  
  
}

## To be copied in the UI
# mod_load_gene_taxonomy_ui("load_gene_taxonomy_ui_1")

## To be copied in the server
# mod_load_gene_taxonomy_server("load_gene_taxonomy_ui_1")
