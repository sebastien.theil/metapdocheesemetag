#' load_kegg UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList
#' @importFrom DT dataTableOutput renderDataTable
#' @importFrom plotly plotlyOutput renderPlotly
#' @importFrom dplyr %>% select select_if
#' @importFrom heatmaply heatmaply
#' @importFrom stringr str_detect
#' @import futile.logger
#' 
mod_load_kegg_ui <- function(id){
  ns <- NS(id)
  
  tagList(
    fluidPage(
      fluidRow(
          DT::dataTableOutput(ns('kegg_datatable')),
          downloadButton(outputId = ns("kegg_download"), label = "Download KEGG table")
      ),
      fluidRow(
        box(width = 6,
          tags$h3("Plot size"),
          sliderInput(ns('plot_height'), label = 'Plot Height', min = 1000, max = 3000, step = 100, value = 1500),
          sliderInput(ns('plot_width'), label = 'Plot Width', min = 1000, max = 3000, step = 100, value= 1000),
          shinyBS::bsButton(inputId = ns('launch_kegg_heatmap'), label = "Create Heatmap", block = F, style = 'danger', type='action'),
          shinyWidgets::multiInput(
            inputId = ns('sample_metadata'),
            label = 'Metadata',
            choices = 1
          )
        )
      )
    ),
    uiOutput(ns('kegg_ui')),
    fluidPage(
      fluidRow(
        box(title = "Settings:", width = 12, status = "warning", solidHeader = TRUE,
          shinyWidgets::pickerInput(
            inputId = ns('factor'),
            label = "Select metadata factor to test :",
            choices = '',
            options = list(
              `live-search` = TRUE)
          ),
          column(10, 
                 shinyWidgets::pickerInput(
                   inputId = ns('cond1'),
                   label = "Select condition 1 :",
                   choices = ''
                 )
          ),
          column(10, 
                 shinyWidgets::pickerInput(
                   inputId = ns('cond2'),
                   label = "Select condition 2 :",
                   choices = ''
                 )
          ),
          shinyWidgets::actionBttn(inputId = ns('launch_deseq'), label = "launch DESEQ2", style = "material-flat")
        ),
        fluidRow(
          box(title = "DESEQ result table", width = 12, solidHeader = TRUE,
              DT::dataTableOutput(ns("deseqTab"))
          )
        ),
        fluidRow(
          box(title = 'PCA plot', width = 12,
              plotOutput(ns('pcaplot'))
          )
        ),
        fluidRow(
          box(title = "DESEQ KEGG description table", width = 12, solidHeader = TRUE,
              DT::dataTableOutput(ns("deseqDesc"))
          )
        )
      )
    )
  )
}
    
#' load_kegg Server Functions
#'
#' @noRd 
mod_load_kegg_server <- function(input, output, session, r){
  ns <- session$ns

  observe({
    req(r$submetadata())
    flog.info('updateMultiInput')
    shinyWidgets::updateMultiInput(session = session,
                                   inputId = 'sample_metadata',
                                   choices = colnames(r$submetadata()),
                                   selected = 'AOP')
    flog.info('update done.')
  })

  
  observe({
    req(r$submetadata())
    flog.info('update factor picker')
    shinyWidgets::updatePickerInput(
      session = session,
      inputId = 'factor',
      choices = colnames(r$submetadata())
    )
  })
  
  observe({
    req(r$submetadata(), input$factor)
    flog.info('update cond1 picker')
    shinyWidgets::updatePickerInput(
      session = session,
      inputId = 'cond1',
      choices = unique(as.vector(dplyr::pull(r$submetadata(), input$factor)))
    )
  })
  
  observe({
    req(r$submetadata(), input$factor, input$cond1)
    conds <- unique(as.vector(dplyr::pull(r$submetadata(), input$factor)))
    choices2 <- conds[conds != input$cond1]
    flog.info('update cond2 picker')
    shinyWidgets::updatePickerInput(
      session = session,
      inputId = 'cond2',
      choices = choices2
    )
  })
  
  kegg_metadata <- reactive({
    tt <- vroom::vroom(file = 'inst/data/kegg_metadata.csv', delim = "\t")
    return(tt)
  })
  

  kegg_data <- reactive({
    tt <- vroom::vroom(file = 'inst/data/kegg_table.csv', delim = "\t")
    
    tt <- tt %>% dplyr::mutate_if(is.character, as.factor)
    
    tt[,unlist(lapply(tt, is.numeric))] <- tt[,unlist(lapply(tt, is.numeric))] %>% replace(is.na(.), 0)
    colsnames <- colnames(tt)
    no_AOP_cols <- stringr::str_detect(colsnames, "AOP")
    no_AOP_cols <- colsnames[!no_AOP_cols]
    selected.cols <- c(no_AOP_cols, as.vector(r$submetadata()$sample.id))
    tt <- tt %>% dplyr::select(selected.cols)
    sup.zero <- tt %>% dplyr::select_if(is.numeric) %>% rowSums() > 0
    tt <- tt[sup.zero,]
    tt <- tt %>% select( -c('lvl1', 'lvl2', 'lvl3', 'lvl4') )
    tt <- tt %>% dplyr::distinct(KO, .keep_all = TRUE) %>% tidyr::drop_na(KO)
    return(tt)
  })
  
  output$kegg_datatable <- DT::renderDataTable({
    kegg_data()
  }, filter="top",options = list(pageLength = 10, scrollX = TRUE, server=TRUE))
  
  
  output$kegg_download <- downloadHandler(
    filename = 'kegg_metag.csv',
    content = function(file){
      req(kegg_data())
      tt <- kegg_data()
      tt <- tt %>% dplyr::slice(input$kegg_datatable_rows_all)
      write.table(tt, file, sep = "\t", row.names = F)
    }
  )
  
  heattable <- reactive({
    flog.info('create heat table')
    res <- list()
    
    tt <- kegg_data()
    tt <- tt %>% tibble::column_to_rownames(var = 'KO')
    
    tt <- tt %>% dplyr::slice(input$kegg_datatable_rows_all)

    tt <- tt %>% select( which (!colSums(tt, na.rm=TRUE) %in% 0))

    flog.info('done heatmap table')
    return(tt)
  })
  
  
  plot_height <- reactive({
    return(input$plot_height)
  })
  
  
  plot_width <- reactive({
    return(input$plot_width)
  })
  
  
  observeEvent(input$launch_kegg_heatmap, {
    output$kegg_ui <- renderUI({
      plotlyOutput(ns('kegg_heatmap'), height = paste(plot_height(), 'px', sep = ''), width = paste(plot_width(), 'px', sep=''))
    })
  })
  
  
  create_heatmap <- eventReactive(input$launch_kegg_heatmap, {
    tt <- heattable()
    num.table <- log(tt %>% select(where(is.numeric)) + 0.00000001)
    # rownames(num.table) <- dplyr::pull(tt,'lvl4')
    heatmaply::heatmaply(num.table,
                         col_side_colors = r$submetadata() %>% dplyr::filter(sample.id == colnames(tt %>% dplyr::select_if(is.numeric))) %>% dplyr::select_at(input$sample_metadata))
  })
  
  
  output$kegg_heatmap <- renderPlotly({
    withProgress(message = 'Computing heatmap...',{
      create_heatmap()
    })
  })
  
  launch_deseq2 <- eventReactive(input$launch_deseq,{
    req(r$submetadata(), input$factor, input$cond1, input$cond2,  heattable())
    # select samples matching conditions
    withProgress({
      setProgress(value = 0, detail = 'loading GO data...')
      dt <- heattable()
      setProgress(value = 10, detail = 'done.')
      
      setProgress(value = 11, detail = 'mutate in integers...')
      dt <- dplyr::mutate_all(dt, as.integer)
      setProgress(value = 15, detail = 'done.')
      
      setProgress(value = 16, detail = 'format data in DESeq2 format...')
      count <- as.matrix(dt)
      metadata <- r$submetadata() %>% select(input$factor)
      
      fun <- glue::glue("dds <- DESeq2::DESeqDataSetFromMatrix(countData = count, colData = metadata, design=~{input$factor})")
      eval(parse(text=fun))
      setProgress(value = 20, detail = 'done.')
      
      setProgress(value = 21, detail = 'DESeq2...')
      dds <- DESeq2::DESeq(dds)
      setProgress(value = 90)
      res <- DESeq2::results(dds)
      
      setProgress(value = 100, detail = 'done.')
    }, message = "computing DESeq2...", min = 0, max = 100)
    return(list('dds' = dds, 'res' = res))
  })
  
  get_deseq_table <- reactive({
    req(launch_deseq2())
    res <- launch_deseq2()$res
    res <- as.data.frame(res[order(res$padj),])
    return(res)
  })
  
  output$deseqTab <- DT::renderDataTable({
    get_deseq_table()
  }, options = list(scrollX = TRUE))
  
  output$pcaplot <- renderPlot({
    dds <- launch_deseq2()$dds
    vsdata <- DESeq2::vst(dds, blind=FALSE)
    DESeq2::plotPCA(vsdata, intgroup=input$factor)
  })
  
  get_diff_description <- reactive({
    dt <- get_deseq_table()
    dt <- dt %>% dplyr::filter(pvalue <= 0.05)
    k_metadata <- kegg_metadata()
    browser()
    diff_desc <- k_metadata %>% dplyr::filter(KO %in% rownames(dt))
  })
  
  output$deseqDesc <- DT::renderDataTable({
    get_diff_description()
  }, options = list(scrollX = TRUE))
  
}
    
## To be copied in the UI
# mod_load_kegg_ui("load_kegg_ui_1")
    
## To be copied in the server
# mod_load_kegg_server("load_kegg_ui_1")
